"""
@author: Jocelyn Aznar
@email: contact@jocelynaznar.eu
@licence:GPL3+ et CeCILL 2.1
"""

import argparse
import arbresetstats
import re
import csv

parser = argparse.ArgumentParser(description='Get the data from a CSV file to input into the graph.')
parser.add_argument("--data", "-d", dest='data', type=str, default=False,
                   help="Spécifie le fichier duquel les données sont extraites.")
parser.add_argument("--fichier", "-f", dest='fichier', type=str, default="arbre_corpus",
                   help="Spécifie le nom du fichier dans lequel est écrit le graphique.")
parser.add_argument("--lettre", "-l", dest='lettre', type=str, default=False,
                   help="Restreint aux formes contenant la ou les lettres spécifiées.")
parser.add_argument("--syllabe", "-s", dest='syllabe', type=str, default=False,
                   help="Faire des stats et arbre pour la syllabe")
parser.add_argument("--contexte", "-c", dest='contexte', type=str, default=False,
                   help="""Définit un contexte pour limiter la recherche. Le formalisme
                   de définition du contexte est celui des expressions régulières.""")
parser.add_argument("--couleurs", "-o", dest='couleurs', type=str, default=False,
                   help="""Active ou non l'usage de couleurs pour marquer les lettres recherchées.""")
parser.add_argument("--mots", "-m", dest='mots', type=str, default=False,
                   help="""Si l'option est précisée, la requête est effectuée sur les unités d'intonations
                   et représente les morphèmes à la place des lettres.""")
parser.add_argument("--separateurs", "-sp", dest='separateurs', type=str, default=" .,;«»!?",
                   help="""Définit les caractères qui seront utilisés pour segmenter la chaine de caractères fournie par l'utilisateur.""")
parser.add_argument("--source_fichier", "-sf", dest='source_fichier', type=str, default=None,
                   help="""Indique le nom du fichier texte sur lequel sera appliqué l'algorithme.""")
parser.add_argument("--source_chaine", "-sc", dest='source_chaine', type=str, default=None,
                   help="""Indique une chaine de caractères sur lequel sera appliqué l'algorithme.""")


args = parser.parse_args()

##### extrait les noms du fichiers CSV pour en faire une liste []
arbresetstats.log_stats("args.fichier", args.fichier)
arbresetstats.log_stats(args.fichier, args.fichier)


liste_formes = []

if args.data:
    with open(args.data, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            liste_formes.append((row[0], int(row[1])))

elif args.source_fichier:
    ### test if the user has specified a text file instead of a CSV file
    with open(args.source_fichier) as fichier:
        lignes = fichier.readlines() 
        for ligne in lignes:
            separateurs = '|'.join(args.separateurs [i:i + 1] for i in range(0, len(args.separateurs ), 1))
            separateurs = "(" + separateurs + ")"
            liste_formes_bruts = re.split(separateurs, ligne)
            for element in liste_formes_bruts:
                if len(element) < 1:
                    continue
                liste_formes.append((element, 1))
 
elif args.source_chaine:
    separateurs = '|'.join(args.separateurs [i:i + 1] for i in range(0, len(args.separateurs ), 1))
    separateurs = "(" + separateurs + ")"
    liste_formes_bruts = re.split(separateurs, args.source_chaine)
    for element in liste_formes_bruts:
        if len(element) < 1:
            continue
        liste_formes.append((element, 1))
        
else:
    print("Please specify the either .csv file, a text file or a string.")

arbresetstats.navigation_arbre_lettre(liste_formes, args.fichier, args.couleurs, args.contexte, args.lettre, args.syllabe)